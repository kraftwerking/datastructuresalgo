package com.kraftwerking.datastructuresalgo.datastructurescourse;

class TreeNode {
    public int key;
    public TreeNode left, right;
    public TreeNode(int item) {
        key = item;
        left = right = null;
    }
}
